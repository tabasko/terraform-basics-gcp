#Tyyppi "resurssi" "omavalintainen nimi"
data "google_compute_subnetwork" "my-subnetwork" {
  name   = "default"
}

data "google_compute_zones" "available" {
}

resource "google_compute_instance" "default" {
  name         = var.name
  machine_type = var.machine_type
  zone         = data.google_compute_zones.available.names[0]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = data.google_compute_subnetwork.my-subnetwork.id
    access_config {
      // Ephemeral IP
    }
  }
}

resource "google_compute_attached_disk" "default" {
  disk     = google_compute_disk.hamk-lisalevy.id
  instance = google_compute_instance.default.id
}

resource "google_compute_disk" "hamk-lisalevy" {
  name  = "lisalevy"
  type  = "pd-ssd"
  zone  = data.google_compute_zones.available.names[0]
}
