variable "project_id" {
  default = "hamk-intro"
}

variable "region" {
  default = "europe-north1"
  description = "Region where we want to place our resources"
}

variable "name" {
  default = "hamk"
  description = "Our instance name"
}

variable "machine_type" {
  default = "f1-micro"
  description = "Our instance machine type"
}